import React from 'react'

import ArtistTableRow from './ArtistTableRow'

/**
 * Renders the artist table
 *
 * @param props Props object which contains an array of artists
 * @return {*}
 */
const ArtistTable = props => (
    <div className="col-sm-8 offset-sm-2">
        <table className="table table-hover">
            <thead>
            <tr>
                <th>Artist</th>
                <th>Genre</th>
                <th>Options</th>
            </tr>
            </thead>
            <tbody>
                {
                    props.artists.map((artist, index) => <ArtistTableRow key={index} artist={artist} />)
                }
            </tbody>
        </table>
    </div>
);

export default ArtistTable;