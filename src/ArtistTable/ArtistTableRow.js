import React from 'react'
import PubSub from 'pubsub-js'

import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import { faItunes } from '@fortawesome/fontawesome-free-brands'
import { faThList } from '@fortawesome/fontawesome-free-solid'

/**
 * Table row containing artist information, button which publishes a toggleArtistModal message and a button which links
 * to the artists iTunes page.
 *
 * @param props Props object which contains an artist member
 * @return {*}
 */
const ArtistTableRow = props => (
    <tr>
        <td>{props.artist.artistName}</td>
        <td>{props.artist.primaryGenreName}</td>
        <td>
            <div className="btn-group">
                <button className="btn btn-info" onClick={() => {PubSub.publish('toggleArtistModal', props.artist);}}>
                    <FontAwesomeIcon icon={faThList} /> Albums
                </button>
                <a className="btn btn-info" href={props.artist.artistLinkUrl} target="_blank">
                    <FontAwesomeIcon icon={faItunes} /> iTunes
                </a>
            </div>
        </td>
    </tr>
);


export default ArtistTableRow;