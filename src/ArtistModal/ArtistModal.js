import axios from 'axios'
import jquery from 'jquery'
import PubSub from 'pubsub-js'
import React, { Component } from 'react'

import AlbumCardCollection from "../AlbumCardCollection/AlbumCardCollection"

let $ = jquery;

/**
 * Artist modal component
 */
class ArtistModal extends Component {

    /**
     * Default constructor which initializes the states to empty and subscribes to the 'toggleArtistModal' message. This
     * modal should be used only once on a page.
     *
     * @param props
     */
    constructor(props) {
        super(props);

        this.state = {
            artist: {},
            albums: []
        };

        PubSub.subscribe('toggleArtistModal', this.toggleArtistModal);
    }

    /**
     * Performs an http get request to the iTunes API to retrieve all albums using the artists iTunes artistId and uses
     * jQuery to display the modal. After retrieving a list of albums, the setAlbums callback is called.
     *
     * @param msg 'toggleArtistModal'
     * @param artist iTunes artist wrapperType
     */
    toggleArtistModal = (msg, artist) => {
        this.setState({
            artist: artist,
            albums: []
        });

        axios.get('https://itunes.apple.com/lookup?id=' + artist.artistId + '&entity=album')
            .then(response => this.setAlbums(response.data.results));

        $('#artistModal').modal('show');
    };

    /**
     * Removes the artist from the collection and sets the albums state
     *
     * @param albums Array of iTunes collection wrapperType objects
     */
    setAlbums(albums) {
        for(let i = 0; i < albums.length; i++) {
            if(albums[i].wrapperType !== 'Collection' && albums[i].collectionType !== 'Album') {
                albums.splice(i, 1);
                i--;
            }
        }

        this.setState({
            albums: [...albums]
        });
    }

    /**
     * Render method
     *
     * @return {*}
     */
    render() {
        return (
            <div id="artistModal" className="modal fade" tabIndex="-1" role="dialog">
                <div className="modal-dialog modal-lg">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Albums by {this.state.artist.artistName}</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <AlbumCardCollection albums={this.state.albums}/>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ArtistModal;