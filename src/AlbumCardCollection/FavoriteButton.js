import PubSub from 'pubsub-js'
import React, { Component } from 'react'

import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import { faHeart } from '@fortawesome/fontawesome-free-solid'

/**
 * Favorite button component
 */
class FavoriteButton extends Component {

    /**
     * Method which stores the collectionId of an album in local storage
     */
    addToFavorites = () => {
        localStorage.setItem(this.props.collectionId, "" + this.props.collectionId);
        this.setState({
            isFavorite: true
        });
    };

    /**
     * Method which removes the collectionId of an album from local storage
     */
    removeFromFavorites = () => {
        localStorage.removeItem(this.props.collectionId);
        this.setState({
            isFavorite: false
        });
        PubSub.publish('favoriteRemoved', this.props.collectionId);
    };

    /**
     * Render method. In case that the album's collectionId has been stored in local storage the icon will be displayed/
     * behave differently in comparison to the case that it is not.
     *
     * @return {*}
     */
    render() {

        if(localStorage.getItem(this.props.collectionId) !== null)
            return (
                <button className="btn btn-sm btn-danger float-sm-right"
                        onClick={this.removeFromFavorites}><FontAwesomeIcon icon={faHeart} /></button>
            );
        else {
            return (
                <button className="btn btn-sm btn-outline-danger float-sm-right"
                        onClick={this.addToFavorites}><FontAwesomeIcon icon={faHeart} /></button>
            );
        }
    }
}

export default FavoriteButton;