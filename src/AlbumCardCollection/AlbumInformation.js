import React from 'react'

/**
 * Album information
 *
 * @param props Props object with an album member of iTunes wrapperType collection
 * @return {*}
 */
const AlbumInformation = props => (
    <div>
        <p>Artist Name: {props.album.artistName}</p>
        <p>Genre: {props.album.primaryGenreName}</p>
        <p>No. Tracks: {props.album.trackCount}</p>
        <p>Release Date: {props.album.releaseDate.substring(0, props.album.releaseDate.indexOf('T'))}</p>
    </div>
);

export default AlbumInformation;