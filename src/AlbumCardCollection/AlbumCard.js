import React from 'react'

import AlbumInformation from './AlbumInformation'
import AlbumTracksTable from './AlbumTracksTable'
import FavoriteButton from './FavoriteButton'

import './AlbumCard.css'

/**
 * Album card component
 *
 * @param props Props object with album member
 * @return {*}
 */
const AlbumCard = props => (
    <div className="card">
        <div className="card-header">
            {props.album.collectionName}
            <FavoriteButton collectionId={props.album.collectionId}/>
        </div>
        <div className="row ">
            <div className="col-md-4">
                <img src={props.album.artworkUrl100.replace('100x100', '400x400')} alt="" className="w-100"/>
            </div>
            <div className="col-md-8 album-nav-container">
                <nav>
                    <div className="nav nav-tabs" id="nav-tab" role="tablist">
                        <a className="nav-item nav-link active" id="album-info-tab" data-toggle="tab" href="#albumInfo" role="tab" aria-controls="album-info" aria-selected="true">Album Info</a>
                        <a className="nav-item nav-link" id="tracks-tab" data-toggle="tab" href="#tracks" role="tab" aria-controls="tracks" aria-selected="false">Tracks</a>
                    </div>
                </nav>
                <div className="tab-content" id="nav-tabContent">
                    <div className="tab-pane fade show active album-tab-content" id="albumInfo" role="tabpanel" aria-labelledby="album-info-tab">
                        <AlbumInformation album={props.album}/>
                    </div>
                    <div className="tab-pane fade" id="tracks" role="tabpanel" aria-labelledby="tracks-tab">
                        <AlbumTracksTable album={props.album}/>
                    </div>
                </div>
            </div>
        </div>
    </div>
);

export default AlbumCard;