import React from 'react'

/**
 * Album track row component
 *
 * @param props Props object with a track member
 * @return {*}
 */
const AlbumTracksRow = props => (
    <tr>
        <td>{props.track.trackNumber}</td>
        <td>{props.track.trackName}</td>
    </tr>
);

export default AlbumTracksRow;