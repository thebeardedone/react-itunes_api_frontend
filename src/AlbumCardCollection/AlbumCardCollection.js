import React, { Component } from 'react'
import AlbumCard from './AlbumCard'

/**
 * Album card collection component
 */
class AlbumCardCollection extends Component {

    /**
     * Render method
     *
     * Expects albums to be provided. If the list of albums is empty, then a message is show indicating that no albums
     * are currently available.
     *
     * @return {*}
     */
    render() {
        if(this.props.albums.length > 0) {
            return (
                <div>
                    {
                        this.props.albums.map((album, index) => <AlbumCard key={index} album={album}/>)
                    }
                </div>
            );
        } else {
            return (
                <p>No albums currently available</p>
            );
        }
    }
}

export default AlbumCardCollection;