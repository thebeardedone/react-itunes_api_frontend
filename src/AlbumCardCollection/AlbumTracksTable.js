import axios from 'axios/index'
import React, { Component } from 'react'

import AlbumTracksRow from './AlbumTrackRow'

/**
 * Album tracks table component
 */
class AlbumTracksTable extends Component {

    /**
     * Default constructor which initializes the state and performs an http get request to the iTunes API for all songs
     * from an album using its collectionId
     *
     * @param props Props object with tracks member
     */
    constructor(props) {
        super(props);

        this.state = {
            tracks: []
        };

        axios.get('https://itunes.apple.com/lookup?id=' + this.props.album.collectionId + '&entity=song')
            .then(response => this.setTracks(response.data.results));
    }

    /**
     * Setter for the tracks state member. The response from the iTunes API includes the collection in the response,
     * this collection object is removed prior to setting the state.
     *
     * @param tracks
     */
    setTracks(tracks) {
        for(let i = 0; i < tracks.length; i++) {
            if(tracks[i].wrapperType !== 'track' && tracks[i].kind !== 'song') {
                tracks.splice(i, 1);
                i--;
            }
        }

        this.setState({
            tracks: [...tracks]
        });
    }

    /**
     * Render method. If the tracks collection is empty, a loading message is displayed.
     *
     * @return {*}
     */
    render() {
        if(this.state.tracks.length === 0) {
            return (
                <div className="text-center">Loading...</div>
            );
        } else {
            return (
                <table className="table table-sm">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Track Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        this.state.tracks.map((track, index) => <AlbumTracksRow key={index} track={track}/>)
                    }
                    </tbody>
                </table>
            );
        }
    }
}

export default AlbumTracksTable;