import axios from 'axios/index'
import jquery from 'jquery'
import PubSub from 'pubsub-js'
import React, { Component } from 'react'

import AlbumCardCollection from '../AlbumCardCollection/AlbumCardCollection'

let $ = jquery;

/**
 * Favorites modal component
 */
class FavoritesModal extends Component {

    /**
     * Default constructor
     *
     * Initializes the state and subscribes to the 'toggleFavoritesModal', 'favoritesRemoved' message. This
     * modal should be used only once on a page.
     *
     * @param props
     */
    constructor(props) {
        super(props);

        this.state = {
            currentArtistId: '',
            artists: [],
            albums: [],
            filteredAlbums: []
        };

        PubSub.subscribe('toggleFavoritesModal', this.toggleFavoritesModal);
        PubSub.subscribe('favoriteRemoved', this.removeAlbum);
    }

    /**
     * Uses jQuery to toggle the modal and call the update data method
     */
    toggleFavoritesModal = () => {
        $('#favoritesModal').modal('show');
        this.updateData();
    };

    /**
     * Adds an album to the favorites list and also adds the artist to the artists list if the artist is not present.
     * This method should be used as a callback when retrieving the list of favorite albums.
     *
     * @param album instance of the iTunes collection wrapperType
     */
    addAlbumAndArtist(album) {
        this.setState({
            albums: [...this.state.albums, album],
            filteredAlbums: [...this.state.filteredAlbums, album]
        });

        let artists = [];

        for(let albumIndex = 0; albumIndex < this.state.albums.length; albumIndex++) {
            let found = false;
            for(let artistIndex = 0; artistIndex < artists.length; artistIndex++) {
                if (this.state.albums[albumIndex].artistId === artists[artistIndex].artistId)
                    found = true;
            }
            if(!found) {
                artists.push({
                    artistId: this.state.albums[albumIndex].artistId,
                    artistName: this.state.albums[albumIndex].artistName
                });
            }
        }

        this.setState({
            artists: [{artistId: '', artistName: ''}, ...artists]
        });
    }

    /**
     * Callback method for when an album is removed from the favorites list.
     *
     * The album is removed from both the albums and filtered albums collections. If this was the last album for an
     * artist in the favorite collection, the artist is removed from the artists collection. If the current artist was
     * selected and their last album was removed, the selection is set to the default value '', .ie the filter is reset.
     *
     * @param msg 'favoriteRemoved'
     * @param collectionId iTunes collectionId
     */
    removeAlbum = (msg, collectionId) => {
        this.setState({
            albums: [...FavoritesModal.removeAlbumFromCollection(this.state.albums, collectionId)],
            filteredAlbums: [...FavoritesModal.removeAlbumFromCollection(this.state.filteredAlbums, collectionId)]
        });

        let artists = this.state.artists;

        for(let i = 0; i < artists.length; i++) {
            let found = false;
            for(let j = 0; j < this.state.albums.length; j++) {
                if(artists[i].artistId === '' || artists[i].artistId === this.state.albums[j].artistId)
                    found = true;
            }

            if(!found) {
                if(artists[i].artistId === parseInt(this.state.currentArtistId, 10))
                    this.filterArtist({target: {value: ''}});
                artists.splice(i, 1);
                i--;
            }
        }

        this.setState({
            artists: artists
        });
    };

    /**
     * Removes a single album from the collection by its collectionId
     *
     * @param collections Collection of albums
     * @param collectionId Id of album to remove
     * @return {Array} Collection without the album
     */
    static removeAlbumFromCollection(collections, collectionId) {
        let albums = collections;

        for(let i = 0; i < albums.length; i++) {
            if(albums[i].collectionId === collectionId) {
                albums.splice(i, 1);
                i--;
            }
        }

        return albums;
    }

    /**
     * Updates the filteredAlbum state depending on the currently selected artist. If the provided artistId is '', then
     * all albums are displayed.
     *
     * @param event
     */
    filterArtist = (event) => {
        if(event.target.value === '') {
            this.setState({
                currentArtistId: event.target.value,
                filteredAlbums: this.state.albums
            });
        } else {
            let filteredAlbums = [];

            for(let i = 0; i < this.state.albums.length; i++) {
                if(this.state.albums[i].artistId === parseInt(event.target.value, 10))
                    filteredAlbums.push(this.state.albums[i]);
            }

            this.setState({
                currentArtistId: event.target.value,
                filteredAlbums: filteredAlbums
            });
        }
    };

    /**
     * Resets all state members to empty and performs http requests for all albums by id to the iTunes API. For each
     * response, the callback addAlbumAndArtist is called.
     */
    updateData() {
        this.setState({
            currentArtistId: '',
            artists: [],
            albums: [],
            filteredAlbums: []
        });

        for (let collectionId in localStorage) {
            if(localStorage.hasOwnProperty(collectionId)) {
                axios.get('https://itunes.apple.com/lookup?id=' + collectionId + '&entity=album')
                    .then(response => this.addAlbumAndArtist(response.data.results[0]));
            }
        }
    }

    /**
     * Render method
     *
     * @return {*}
     */
    render() {
        return (
            <div id="favoritesModal" className="modal fade" tabIndex="-1" role="dialog">
                <div className="modal-dialog modal-lg">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Your Favorites</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <div className="form-group">
                                <label htmlFor="artistSelection">Filter by Author:</label>
                                <select className="form-control" id="artistSelection" value={this.state.currentArtistId} onChange={this.filterArtist}>
                                    {
                                        this.state.artists.map((artist, index) => <option key={index} value={artist.artistId}>{artist.artistName}</option>)
                                    }
                                </select>
                            </div>
                            <AlbumCardCollection albums={this.state.filteredAlbums}/>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default FavoritesModal;