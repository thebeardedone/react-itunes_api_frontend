import axios from 'axios'
import PubSub from 'pubsub-js'
import React, { Component } from 'react'

import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import { faHeart, faSearch } from '@fortawesome/fontawesome-free-solid'

import ArtistTable from './ArtistTable/ArtistTable'
import ArtistModal from './ArtistModal/ArtistModal'
import FavoritesModal from './FavoritesModal/FavoritesModal'

/**
 * Main app
 */
export default class App extends Component {

    /**
     * Default constructor for application.
     *
     * Initializes the artist and artists members
     *
     * @param props
     * @public
     **/
    constructor(props) {
        super(props);
        this.state = {
            artist: '',
            artists: []
        };
    }

    /**
     * Callback method which updates the artist state
     *
     * @param event
     * @public
     **/
    onSearchParameterChange = (event) => {
        this.setState({ artist: event.target.value });
    };

    /**
     * Artists state setter
     *
     * @public artist Array of artists
     * @public
     **/
    setArtists(artists) {
        this.setState({
            artists: [...artists]
        });
    }

    /**
     * Callback which queries the iTunes API for a list of artists based on the current artist state
     *
     * @param event
     * @public
     **/
    onSearchSubmit = (event) => {
        event.preventDefault();
        axios.get('https://itunes.apple.com/search?term=' + this.state.artist + '&country=US&media=music&entity=musicArtist')
            .then(response => this.setArtists(response.data.results));
    };

    /**
     * Static method which publishes a 'toggleFavoritesModal' event.
     *
     * @public
     **/
    static showFavorites() {
        PubSub.publish('toggleFavoritesModal', null);
    }

    /**
     * Render method
     *
     * @return {*}
     **/
    render() {
        return (
            <div>
                <nav className="navbar navbar-light bg-light">
                    <div className="col-sm-8 offset-sm-2">
                        <label className="sr-only" htmlFor="inlineFormInputGroup">Username</label>
                        <div className="input-group mb-2">
                            {/* Catch event when the enter key is pressed and submit the form */}
                            {/* eslint-disable no-unused-expressions */}
                            <input type="text" className="form-control" placeholder="Search for an Artist"
                                   value={this.state.artist} onChange={this.onSearchParameterChange}
                                   onKeyPress={(event) => {event.key === 'Enter' ? this.onSearchSubmit(event) : null}} />
                            <div className="input-group-append">
                                <button className="btn btn-primary" onClick={this.onSearchSubmit}><FontAwesomeIcon icon={faSearch} /></button>
                                <button className="btn btn-danger" onClick={App.showFavorites}><FontAwesomeIcon icon={faHeart} /></button>
                            </div>
                        </div>
                    </div>
                </nav>
                <div className="container-fluid">
                    <FavoritesModal/>
                    <ArtistModal/>
                    <ArtistTable artists={this.state.artists} />
                </div>
            </div>
        );
    }
}