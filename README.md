This is a repository containing the solution to the provided [technical assessment](https://gitlab.com/thebeardedone/react-itunes_api_frontend/raw/master/Senior%20Frontend%20Developer%20Assignment.pdf)
 
## Table of Contents

- [Design Decisions](#design-decisions)
- [Implementation Decisions](#implementation-decisions)
- [Available Scripts](#available-scripts)
  - [npm start](#npm-start)
  - [npm test](#npm-test)
  - [npm run build](#npm-run-build)
- [Deployment](#deployment)
  - [Docker](#docker)
  - [serve](#serve)

## Design Decisions

Since no artist images are provided by the iTunes API, I decided to use a table to display the artists after the search.
Although there exist several hacks to retrieve images from an S3 bucket where iTunes stores the images, I found this
workaround to be too far out of the scope of the assignment and decided not to implement it. This is one area where the
application could be visually improved. Since the table looked empty, I decided to add a link to the artists iTunes
profile.

The favorites and albums of an artist are displayed in modals as they are clean due to the information being limited.
Additionally, I decided to add tabs which display the tracks of an album.

## Implementation Decisions

No test cases were required and therefore none were written.

For the communication between components PubSubJS was used in order to implement a publisher subscriber pattern.

Although there are libraries such as react-bootstrap, I used the regular variants of libraries to get a hang of react.

Because the artist album modal and favorite modal display the same information, all of the components were reused for
the display of the albums.

The favorites list is stored in localstorage for the sake of simplicity.

## Available Scripts

Before running any of the scripts, one must install all of the dependencies by running:

```
npm install
```

### npm start

To run the debug build, run the following command:

```
npm start
```

### npm test

Since the assessment did not require any test cases, only the default react test case has been provided which checks
whether the application does not crash on startup.

```
npm test
```

### npm run build

To create a production ready optimized build run:

```
npm run build
```

These artifacts may then be served statically from a host.

## Deployment

Two easy ways to get the application to run is to either use the Dockerfile provided to create an image or to use serve.

### Docker

To build the image run:

```
docker build -t itunes_api_frontend .
```

To start it run:

```
docker run -p 5000:5000 -it itunes_api_frontend
```

The application should now be accessible on localhost through port 5000.

### serve

First build the production ready artifacts using [npm run build](#npm-run-build), install serve globally by running:

```
npm install -g serve
```

Then serve the build directory statically:

```
serve -s build
```

The application should now be accessible on localhost through port 5000.