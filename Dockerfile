FROM node:9.5.0-alpine

RUN npm install -g serve

ADD . /app

WORKDIR /app

RUN npm install

RUN npm run build

ENTRYPOINT serve -s /app/build